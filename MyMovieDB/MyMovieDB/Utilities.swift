//
//  Utilities.swift
//  MyMovieDB
//
//  Created by akis vavouranakis on 25/09/2018.
//  Copyright © 2018 akis vavouranakis. All rights reserved.
//

import Foundation
import UIKit

class Utilities{
    
    /**
     Gets a date(String) in "yyyy-MM-dd" format and returns a date(String) in "dd MMMM yyyy" format.
     - Parameters:
     - date: A date in String
     */
    static func convertDate(_ date: String) -> String {
        let entryDate = date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd MMMM yyyy"
        if let newDate = date {
            return  dateFormatter.string(from: newDate)
        }
        else {
            return entryDate
        }
        
    }
    
}
