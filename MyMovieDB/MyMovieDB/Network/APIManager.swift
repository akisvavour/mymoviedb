//
//  APIManager.swift
//  MyMovieDB
//
//  Created by akis vavouranakis on 22/09/2018.
//  Copyright © 2018 akis vavouranakis. All rights reserved.
//

import Foundation
import UIKit

/// The available API Calls
class APIManager: NSObject {
    
    let baseURL = "https://api.themoviedb.org/3"
    let apiKey = "6b2e856adafcc7be98bdf0d8b076851c"
    static let sharedInstance = APIManager()
    static let getSearchEndpoint = "/search/multi"
    static let getMovieEndpoint = "/movie/"
    static let getTVEndpoint = "/tv/"

    
    /**
     Get shows by a text query. Search multiple models in a single request. Multi search currently supports searching for movies, tv shows and people in a single request.
     - Parameters:
     - text: Pass a text query to search. This value should be URI encoded.
     - page: Specify which page to query.
     - onSuccess: the callback function to call when the API Call returns valid data.
     - onFailure: the callback function to call when the API Call returns error or invalid data.
     */
    func getSearchWith(text: String, page: Int, onSuccess: @escaping([Show]) -> Void, onFailure: @escaping(Error) -> Void){
        
        let originalString : String = baseURL + APIManager.getSearchEndpoint + "?api_key=\(apiKey)&language=en-US&query=\(text)&page=\(String(page))&include_adult=false"
        let urlString = originalString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let request: NSMutableURLRequest = NSMutableURLRequest(url: NSURL(string: urlString!)! as URL)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error -> Void in
            if(error != nil){
                onFailure(error!)
            }
            else {
                
                guard  let data = data else {
                    return
                }
    
                do {
                    //let responseData = String(data: data, encoding: String.Encoding.utf8)
                    //print("Search data:\(responseData)")
                    let apiResult = try JSONDecoder().decode(Shows.self, from: data)
                    onSuccess(apiResult.results)
                }catch let jsonErr {
                    print("Error serializing json:",jsonErr)
                    onFailure(jsonErr)

                }
            }
        })
        task.resume()
        
    }
    
    
    /**
     Get the genres of a movie by id.
     - Parameters:
     - movie_id: Pass the movie id.
     - onSuccess: the callback function to call when the API Call returns valid data.
     - onFailure: the callback function to call when the API Call returns error or invalid data.
     */
    func getMovieDetailsWith(movie_id: Int,  onSuccess: @escaping([Genre]) -> Void, onFailure: @escaping(Error) -> Void) {
        
        let url : String = baseURL + APIManager.getMovieEndpoint + "\(movie_id)?api_key=\(apiKey)&language=en-US"
        let request: NSMutableURLRequest = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            
            if(error != nil){
                onFailure(error!)
            }
            else {
                
                guard  let data = data else {
                    return
                }
                
                do {
                    let apiResult = try JSONDecoder().decode(MovieDetails.self, from: data)
                    onSuccess(apiResult.genres)
                }catch let jsonErr {
                    print("Error serializing json:",jsonErr)
                    onFailure(jsonErr)
                }
            }
        })
        task.resume()
        
    }
    
    /**
     Get the genres of a tv show by id.
     - Parameters:
     - show_id: Pass the tv show id.
     - onSuccess: the callback function to call when the API Call returns valid data.
     - onFailure: the callback function to call when the API Call returns error or invalid data.
     */
    func getTVShowDetailsWith(show_id: Int,  onSuccess: @escaping([Genre]) -> Void, onFailure: @escaping(Error) -> Void) {
        
        let url : String = baseURL + APIManager.getTVEndpoint + "\(show_id)?api_key=\(apiKey)&language=en-US"
        let request: NSMutableURLRequest = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            
            if(error != nil){
                onFailure(error!)
            }
            else {
                
                guard  let data = data else {
                    return
                }
                
                do {
                    let apiResult = try JSONDecoder().decode(MovieDetails.self, from: data)
                    onSuccess(apiResult.genres)
                }catch let jsonErr {
                    print("Error serializing json:",jsonErr)
                    onFailure(jsonErr)
                }
            }
        })
        task.resume()
        
    }
    
    /**
     Get the videos that have been added to a movie.
     - Parameters:
     - movie_id: Pass the id of movie.
     - onSuccess: the callback function to call when the API Call returns valid data.
     - onFailure: the callback function to call when the API Call returns error or invalid data.
     */
    func getMovieVideos(movie_id: Int,  onSuccess: @escaping([Video]) -> Void, onFailure: @escaping(Error) -> Void) {
        let url: String = "https://api.themoviedb.org/3/movie/\(movie_id)/videos?api_key=\(apiKey)&language=en-US"
        let request: NSMutableURLRequest = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            
            if(error != nil){
                onFailure(error!)
            }
            else {
                
                guard  let data = data else {
                    return
                }
                
                do {
                    let apiResult = try JSONDecoder().decode(Videos.self, from: data)
                    onSuccess(apiResult.results)
                }catch let jsonErr {
                    print("Error serializing json:",jsonErr)
                    onFailure(jsonErr)
                }
            }
        })
        task.resume()
        
    }
    
    /**
     Get the videos that have been added to a TV Show.
     - Parameters:
     - tvShow_id: Pass the tv show id.
     - onSuccess: the callback function to call when the API Call returns valid data.
     - onFailure: the callback function to call when the API Call returns error or invalid data.
     */
    func getTVShowVideos(tvShow_id: Int,  onSuccess: @escaping([Video]) -> Void, onFailure: @escaping(Error) -> Void) {
        let url: String = "https://api.themoviedb.org/3/tv/\(tvShow_id)/videos?api_key=\(apiKey)&language=en-US"
        let request: NSMutableURLRequest = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            
            if(error != nil){
                onFailure(error!)
            }
            else {
                
                guard  let data = data else {
                    return
                }
                
                do {
                    let apiResult = try JSONDecoder().decode(Videos.self, from: data)
                    onSuccess(apiResult.results)
                }catch let jsonErr {
                    print("Error serializing json:",jsonErr)
                    onFailure(jsonErr)
                }
            }
        })
        task.resume()
        
    }
  
}


