//
//  ShowDetailsTableViewController.m
//  MyMovieDB
//
//  Created by akis vavouranakis on 23/09/2018.
//  Copyright © 2018 akis vavouranakis. All rights reserved.
//

#import "ShowDetailsTableViewController.h"
#import "MyMovieDB-Swift.h"

@interface ShowDetailsTableViewController ()

/// The show object
@property (strong, nonatomic) Show *myShow;

@end

@implementation ShowDetailsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.myShow = self.show;
    self.backdropPath = @"";
    if([self.myShow.backdrop_path length] >0) {
        self.backdropPath = self.myShow.backdrop_path;
    }
    self.genresOfShow = self.genres;
    self.mediaType = self.myShow.media_type;
    self.videoURL = self.video;
    // set the sections array
    self.sections = @[@"Image", @"Title", @"Summary", @"Genres", @"Trailer"];
    
    // set the content array based on the media type
    if ([self.mediaType isEqualToString:@"movie"]) {
        self.content = @[self.backdropPath, self.myShow.title, self.myShow.overview, self.genresOfShow, self.videoURL];
    }
    else if ([self.mediaType isEqualToString:@"tv"]) {
        self.content = @[self.backdropPath, self.myShow.name, self.myShow.overview, self.genresOfShow, self.videoURL];
    }
    
    // register cells for tableView
    [self.tableView registerNib:[UINib nibWithNibName:@"ShowImageTableViewCell" bundle:nil] forCellReuseIdentifier:@"imageCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ShowDetailsTableViewCell" bundle:nil] forCellReuseIdentifier:@"simpleCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"VideoTableViewCell" bundle:nil] forCellReuseIdentifier:@"videoCell"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        ShowImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"imageCell" forIndexPath:indexPath];
        [cell updateCellWithImage:self.myShow.backdrop_path];
        [cell setNeedsLayout];
        return cell;
    }
    else if (indexPath.section == 4) {
        VideoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"videoCell" forIndexPath:indexPath];
        if ( [self.videoURL length] != 0 ) {
            NSURL *url = [NSURL URLWithString: self.videoURL];
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
            [cell.myWebView loadRequest:requestObj];
        }
        return cell;
    }
    else {
        ShowDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"simpleCell" forIndexPath:indexPath];
        cell.titleLabel.text = self.sections[indexPath.section] ;
        cell.contentLabel.text = self.content[indexPath.section] ;
        return cell;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 180.0;
    }
    else if (indexPath.section == 4) {
        return 300;
    }
    else {
        return UITableViewAutomaticDimension;
    }
}

@end
