//
//  ShowDetailsTableViewController.h
//  MyMovieDB
//
//  Created by akis vavouranakis on 23/09/2018.
//  Copyright © 2018 akis vavouranakis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShowImageTableViewCell.h"
#import "ShowDetailsTableViewCell.h"
#import "VideoTableViewCell.h"

@interface ShowDetailsTableViewController : UITableViewController

/// The show object passed from SearchCV
@property (strong, nonatomic) id show;
/// The genres of the show
@property (strong, nonatomic) id genres;
/// The video URL in order to load in the web view
@property (strong, nonatomic) id video;

/// The genres of the show
@property (nonatomic, weak, readwrite) NSString* genresOfShow;
/// The media type of the show (movie or tv-show)
 @property (nonatomic, weak, readwrite) NSString* mediaType;
/// The backdrop URL in order to download the backdrop image
@property (nonatomic, weak, readwrite) NSString* backdropPath;
/// The video URL in order to load it in the web view
@property (nonatomic, weak, readwrite) NSString* videoURL;
/// The array of sections of our tableView
@property (nonatomic, strong, readwrite) NSArray* sections;
/// The array of content for every section in our tableView
@property (nonatomic, strong, readwrite) NSArray* content;

@end
