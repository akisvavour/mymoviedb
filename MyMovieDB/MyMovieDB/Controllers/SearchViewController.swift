//
//  ViewController.swift
//  MyMovieDB
//
//  Created by akis vavouranakis on 21/09/2018.
//  Copyright © 2018 akis vavouranakis. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    /// The search bar.
    @IBOutlet weak var searchBar: UISearchBar!
    /// The shows tableView
    @IBOutlet weak var tableView: UITableView!
    /// No results label. We display it, when we have no results from getSearchWith API call.
    @IBOutlet weak var noResultsLabel: UILabel!
    /// Loading View. We display it during the getSearchWith, getMovieDetailsWith, getTVShowDetailsWith API calls.
    @IBOutlet weak var loadingView: UIView!
    // The activity indicator of the loading view
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    /// array with the searched shows
    var shows: [Show] = []
    /// timer to detect if user is typing in the search text field. if user stop typing , timer triggers a getSearchWith API call.
    var searchTimer: Timer?
    /// the genres we pass to ShowDetailsVC
    var genres: String = "No Genres"
    /// A static dictionary in order to save video URLs by show_id
    static var videoURLs: [Int: String] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.addDismissingKeyboard()
        activityIndicator.transform = CGAffineTransform(scaleX: 2, y: 2)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - TableView data source
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shows.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let show = shows[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell") as! SearchTableViewCell
        cell.setShow(show: show)
        return cell
    }
    
    // MARK: - TableView delegate

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let media_type = shows[indexPath.row].media_type {
            
            if media_type == "movie" {
                self.goToMovieDetails(show: shows[indexPath.row])
            }
            else if  media_type == "tv" {
                self.goToTVShowDetails(show: shows[indexPath.row])
            }
            else {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Error", message: "Not a movie or tv show", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showDetails" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let controller = segue.destination as! ShowDetailsTableViewController
                controller.show = self.shows[indexPath.row]
                controller.genres = self.genres
                if let videoURL = SearchViewController.videoURLs[self.shows[indexPath.row].id] {
                    controller.video = videoURL
                } else {
                    controller.video = ""
                }
                
            }
        }
    }
    
    /**
     Go to ShowDetails VC after getting the genres of the movie.
     - Parameters:
     - show: Pass the show object.
     */
    func goToMovieDetails(show: Show) {
        self.loadingView.isHidden = false
        APIManager.sharedInstance.getMovieDetailsWith(movie_id: show.id , onSuccess: { (genres) in
            self.genres = self.getGenres(showGenres: genres)
            DispatchQueue.main.async {
                self.loadingView.isHidden = true
                self.performSegue(withIdentifier: "showDetails", sender: self)
            }
            
        }) { (error) in
            DispatchQueue.main.async {
                self.loadingView.isHidden = true
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
        }
        
    }
    
    /**
     Go to ShowDetails VC after getting the genres of the TV Show.
     - Parameters:
     - show: Pass the show object.
     */
    func goToTVShowDetails(show: Show) {
        self.loadingView.isHidden = false
        APIManager.sharedInstance.getTVShowDetailsWith(show_id: show.id , onSuccess: { (genres) in
            self.genres = self.getGenres(showGenres: genres)
            DispatchQueue.main.async {
                self.loadingView.isHidden = true
                self.performSegue(withIdentifier: "showDetails", sender: self)
            }
            
        }) { (error) in
            DispatchQueue.main.async {
                self.loadingView.isHidden = true
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
        }
        
    }
    
    // MARK: - Get Genres
    
    /**
     Returns the union of show's genres in string.
     - Parameters:
     - showeGenres: Pass the array of show's genres.
     */
    private func getGenres(showGenres: [Genre]) -> String {
        var allGenres = ""
        for genre in showGenres {
            if let last = showGenres.last, last.name == genre.name {
                allGenres += genre.name
            }
            else {
                allGenres += genre.name + ", "
            }
        }
        return allGenres
    }

}




extension SearchViewController: UISearchBarDelegate {

    /**
     Add a single tap gesture recognizer in order to dismiss keyboard.
     */
    private func addDismissingKeyboard() {
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboardTap(sender:)))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(singleTapGestureRecognizer)
    }
    
    /**
     The fired of method of tap gesture recognizer. The gesture recognizer passes itself as the argument.
     - Parameters:
     - sender: the UITapGestureRecognizer.
     */
    @objc func dismissKeyboardTap(sender: UITapGestureRecognizer) {
        self.searchBar.resignFirstResponder()
    }
    
    // MARK: -  Search Bar delegate functions

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count >= 3 {
            if searchTimer != nil {
                searchTimer?.invalidate()
                searchTimer = nil
            }
            searchTimer = Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(searchForShow(_:)), userInfo: searchText, repeats: false)
        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.searchBar.endEditing(true)
    }
    
    /**
     The fired of method of searchTimer Timer in order to call the getSearchWith API call. The timer passes itself as the argument.
     - Parameters:
     - timer: The timer itself.
     */
    @objc func searchForShow(_ timer: Timer) {
        let searchText = timer.userInfo as! String
        print("Search for:",searchText)
        self.loadingView.isHidden = false
        DispatchQueue.global(qos: .background).async {
            
            APIManager.sharedInstance.getSearchWith(text: searchText, page: 1, onSuccess: { searchedShows in
                self.shows = searchedShows
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.loadingView.isHidden = true
                    if self.shows.count > 0 {
                        self.noResultsLabel.isHidden = true
                    }else {
                        self.noResultsLabel.isHidden = false
                    }
                }
                
            }, onFailure: { error in
                
                DispatchQueue.main.async {
                    self.loadingView.isHidden = true
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
            })
            
        }
        
    }

}
