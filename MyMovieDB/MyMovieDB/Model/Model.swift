//
//  Model.swift
//  MyMovieDB
//
//  Created by akis vavouranakis on 22/09/2018.
//  Copyright © 2018 akis vavouranakis. All rights reserved.
//

import Foundation

/// Shows model structure
@objc
class Shows: NSObject, Decodable {
    
    @objc let page: Int
    @objc let results: [Show]
    
    init(page: Int, results: [Show]) {
        self.page = page
        self.results = results
    }
}

/// Show model structure
@objc
class Show: NSObject, Decodable {
    
    @objc let id: Int
    @objc let media_type: String?
    @objc let title: String?
    @objc let name: String?
    @objc let poster_path: String?
    @objc let backdrop_path: String?
    @objc let release_date: String?
    @objc let first_air_date: String?
    let vote_average: Double?
    let video: Bool?
    @objc let overview: String?
    
    
    init(id: Int, media_type: String, title: String?, name: String?, poster_path: String?, backdrop_path: String?, release_date: String?, first_air_date: String?, vote_average: Double?,overview: String?, video: Bool?) {
        self.id = id
        self.media_type = media_type
        self.title = title
        self.name = name
        self.poster_path = poster_path
        self.backdrop_path = backdrop_path 
        self.release_date = release_date
        self.first_air_date = first_air_date
        self.vote_average = vote_average
        self.overview = overview
        self.video = video
    }
    
}

//


/// MovieDetails model structure
struct MovieDetails: Decodable {
    let genres: [Genre]
}

/// Genre model structure
struct Genre: Decodable {
    let id: Int
    let name: String
}



/// Videos model structure
class Videos: NSObject, Decodable {
    
    let results: [Video]
    
    init(results: [Video]) {
        self.results = results
    }
}

/// Video model structure
class Video: NSObject, Decodable {
    
    let name: String?
    let key: String?
    let type: String?

    init(name: String?, key: String?, type: String?) {
        self.name = name
        self.key = key
        self.type = type
    }
}
