//
//  SearchTableViewCell.swift
//  MyMovieDB
//
//  Created by akis vavouranakis on 22/09/2018.
//  Copyright © 2018 akis vavouranakis. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {
    /// The show poster
    @IBOutlet weak var showPosterImageView: UIImageView!
    /// The title of the show
    @IBOutlet weak var showTitleLabel: UILabel!
    /// The average rating of the show
    @IBOutlet weak var showVoteAverageLabel: UILabel!
    /// The release date of the show
    @IBOutlet weak var showReleaseDateLabel: UILabel!
    // the id of the show
    var showId: Int!
    // array with the video URLs of the show
    var videos: [Video]?
    // the image base URL
    let imageBaseURL = "https://image.tmdb.org/t/p/w185_and_h278_bestv2"
    
    /**
     Set the show properies in the SearchTableViewCell.
     - Parameters:
     - show: The show object which we want to display in the cell.
     */
    func setShow(show: Show)  {
        if let posterPath = show.poster_path {
            showPosterImageView.downloadImageFrom(link: imageBaseURL + posterPath)
        }
        
        // check if show type is movie or tv-show
        if show.media_type == "movie" {
            showTitleLabel.text = show.title
            if let date = show.release_date {
                showReleaseDateLabel.text = Utilities.convertDate(date)
            }
            if let hasVideo = show.video, hasVideo == true {
                self.getMovieVideos(movie_id: show.id)
            }
        }
        else if show.media_type == "tv"{
            showTitleLabel.text = show.name
            if let date = show.first_air_date {
                showReleaseDateLabel.text = Utilities.convertDate(date)
            }
            if let hasVideo = show.video, hasVideo == true {
                self.getTVShowVideos(tvShow_id: show.id)
            }
        }
        if let voteAverage = show.vote_average {
            showVoteAverageLabel.text = "Rating: " + String(voteAverage) + "/10"
        }
        
        showId = show.id

    }
    
    /**
     Get the first video URL(if exist) of the returned video URLs(of getMovieVideos API call). We add the video url in the static dictionary videoURLs of SearchViewController class.
     - Parameters:
     - movie_id: Pass the movie id.
     */
    func getMovieVideos(movie_id: Int) {
        
        DispatchQueue.global(qos: .background).async {
            
            APIManager.sharedInstance.getMovieVideos(movie_id: movie_id, onSuccess: { videos in

                self.videos = videos
                if let _videos =  self.videos {
                    if _videos.count > 0 {
                        if let firstVideo = _videos.first {
                            if let videoKey = firstVideo.key {
                                let trailerURL = "https://www.youtube.com/watch?v=" + videoKey
                                SearchViewController.videoURLs[movie_id] = trailerURL
                            }
                        }
                    }
                }
               
                
            }, onFailure: { error in
                
                print("failed getting movie videos")
                
            })
            
        }
    }
    
    /**
    Get the first video URL(if exist) of the returned video URLs(of getTVShowVideos API call). We add the video url in the static dictionary videoURLs of SearchViewController class.
     - Parameters:
     - tvShow_id: Pass the tv Show id.
     */
    func getTVShowVideos(tvShow_id: Int) {
        
        DispatchQueue.global(qos: .background).async {
            
            APIManager.sharedInstance.getTVShowVideos(tvShow_id: tvShow_id, onSuccess: { videos in
                
                self.videos = videos
                if let _videos =  self.videos {
                    if _videos.count > 0 {
                        if let firstVideo = _videos.first {
                            if let videoKey = firstVideo.key {
                                let trailerURL = "https://www.youtube.com/watch?v=" + videoKey
                                SearchViewController.videoURLs[tvShow_id] = trailerURL
                            }
                        }
                    }
                }
                
                
            }, onFailure: { error in
                
                print("failed getting tv-show videos")
                
            })
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }

}

extension UIImageView {
    /**
     Download an image from a url and set it as image to the UIimageView object
     - Parameters:
     - link: Pass the url in string.
     */
    func downloadImageFrom(link: String) {
        URLSession.shared.dataTask( with: NSURL(string:link)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async {
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
    }
}
