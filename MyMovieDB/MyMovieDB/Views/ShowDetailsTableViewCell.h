//
//  ShowDetailsTableViewCell.h
//  MyMovieDB
//
//  Created by akis vavouranakis on 24/09/2018.
//  Copyright © 2018 akis vavouranakis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowDetailsTableViewCell : UITableViewCell
/// the title of the section
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
/// the content of the section
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end
