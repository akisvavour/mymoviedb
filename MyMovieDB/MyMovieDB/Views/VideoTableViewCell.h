//
//  VideoTableViewCell.h
//  MyMovieDB
//
//  Created by akis vavouranakis on 25/09/2018.
//  Copyright © 2018 akis vavouranakis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoTableViewCell : UITableViewCell
/// the web view of the video cell
@property (strong, nonatomic) IBOutlet UIWebView *myWebView;

@end
