//
//  ShowDetailsTableViewCell.m
//  MyMovieDB
//
//  Created by akis vavouranakis on 24/09/2018.
//  Copyright © 2018 akis vavouranakis. All rights reserved.
//

#import "ShowDetailsTableViewCell.h"

@implementation ShowDetailsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
