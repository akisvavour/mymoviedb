//
//  ShowImageTableViewCell.h
//  MyMovieDB
//
//  Created by akis vavouranakis on 24/09/2018.
//  Copyright © 2018 akis vavouranakis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowImageTableViewCell : UITableViewCell
/// The show imageView
@property (weak, nonatomic) IBOutlet UIImageView *showImage;

-(void)updateCellWithImage:(nonnull NSString *) imageURL;


@end
