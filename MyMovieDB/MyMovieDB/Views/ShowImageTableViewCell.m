//
//  ShowImageTableViewCell.m
//  MyMovieDB
//
//  Created by akis vavouranakis on 24/09/2018.
//  Copyright © 2018 akis vavouranakis. All rights reserved.
//

#import "ShowImageTableViewCell.h"

@implementation ShowImageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/**
 Download an image from a url and set it as image to the UIimageView property of ShowImageTableViewCell cell
 - Parameters:
 - imageURL: The url of image in NSString.
 */
-(void)updateCellWithImage:(NSString *)imageURL{
    

        NSString *baseURL = @"https://image.tmdb.org/t/p/w185_and_h278_bestv2" ;
        NSString *finalImageURL = [NSString stringWithFormat:@"%@%@", baseURL, imageURL];
        //NSLog(@"image url is:%@", finalImageURL);
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void) {
            
            NSURL *imgURL = [NSURL URLWithString:finalImageURL];
            NSData *imageData = [NSData dataWithContentsOfURL:imgURL];
            UIImage* image = [[UIImage alloc] initWithData:imageData];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.showImage.image = image;
                    [self setNeedsLayout];
                    
                });
            }
        });

    
    
}

@end
